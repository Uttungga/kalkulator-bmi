import { useState, useMemo } from "react";
import "./App.css";

function App() {
  const [berat, setBerat] = useState(50);
  const [tinggi, setTinggi] = useState(140);

  const UbahBerat = (e: React.ChangeEvent<any>): void => {
    const inputBerat = e.target.value;
    setBerat(inputBerat);
  };

  const UbahTinggi = (e: React.ChangeEvent<any>): void => {
    const inputTinggi = e.target.value;
    setTinggi(inputTinggi);
  };

  const result: any = useMemo(() => {
    const hitungTinggi = tinggi / 100;
    return (berat / (hitungTinggi * hitungTinggi)).toFixed(1);
  }, [berat, tinggi]);

  return (
    <>
      <div className="container">
        <h1>Kalkulator BMI</h1>
        <div className="row-bmi">
          <p>Berat Badan: {berat} kg</p>
          <input
            type="range"
            onChange={UbahBerat}
            className="inputRange"
            step="1"
            min="40"
            max="180"
          />
          <p>Tinggi Badan: {tinggi} cm</p>
          <input
            type="range"
            onChange={UbahTinggi}
            className="inputRange"
            step="1"
            min="140"
            max="200"
          />
        </div>
        <div className="row-result">
          <p className="outputText">Hasil Perhitungan BMI</p>
          <p className="output">{result}</p>
          {/* <p ket={status}></p> */}
        </div>
      </div>
    </>
  );
}

export default App;
